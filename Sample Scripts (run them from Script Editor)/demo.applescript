tell application id "org.tempel.xojo.demo.scriptable" -- AppleScriptable.app
	
	-- This script gets various values without displaying them.
	-- To see the result of a "get" command, end the script by
	-- placing a "return" in the next line, or get the incredibly
	-- versatile "Script Debugger" application which allows you to
	-- single step through the script code and see all the variables.
	
	
	-- create a new window and close it again
	set w to make new window
	close w
	
	get window 1
	get properties of window 1 -- gets all the properties we can access from this script
	
	tell first window
		
		-- date operations
		get the date
		set the date to current date
		
		-- use the rich text operations from the Text Suite:
		set color of second word of rich text editor 1 to {0, 40000, 10000}
		set words 5 thru 7 of rich text editor 2 to "X"
		
	end tell
	
	get title of window 1
	
	-- get and set properties of the City class
	get area of city 1
	set area of city 1 to 5555
	set area of city 2 to 6666
	
	set thelist to {}
	set area of city 1 to 4455
	try
		set name of city 1 to "nix" -- this should give an error as it's read-only
	end try
	
	set end of thelist to name of every country
	set end of thelist to name of every city
	set end of thelist to properties of city 1
	set end of thelist to properties of country 1
	get thelist
	
	get title of windows
	set title of every window to "new title"
	
	get countries
	tell first country
		get name
	end tell
	
	get a reference to name of controls of first window
	
	-- rotate order of countries
	move first country to end
	
	-- refresh list to see new country order
	tell window 1
		tell first listbox
			reload
			set list index to 0
		end tell
	end tell
	
	tell window 1
		get title
		set country list index to (country list index + 1)
		tell listbox "countryList"
			get name
		end tell
		tell countryList
			repeat with row in every listbox row
				tell row
					if value of first listbox cell = "Germany" then
						tell second listbox cell
							if value = "Berlin" then
								set value to "Bonn"
							else
								set value to "Berlin"
							end if
						end tell
					end if
				end tell
			end repeat
			--make new listbox row
		end tell
	end tell
	
	-- add a city if it's not there yet
	if name of every city does not contain "Bonn" then
		make new city at beginning with properties ¬
			{name:"Bonn", country name:"Germany", area:141, population:314000}
	end if
	
	-- delete a particular city if it exists
	delete (every city whose name is "Munich")
	
	-- get the last city, which would change with each run due to the rotation above
	get last city
	
	(* The following deals with immediate (non-proxy) classes, i.e. those 
 	   where the values are directly stored inside their properties and that
	   are held permanently in the app.
	   They are the easiest to use because adding and removing items
	   from such elements requires no extra coding effort.
	*)
	make new person with properties {name:"Paul", gender:"m", age:34}
	make new person at beginning with properties {name:"Ines", gender:"f", age:30}
	duplicate first person to end with properties {age:31}
	delete first person
	get properties of persons
	delete persons
	get properties of persons
	
	tell window 1
		get a file
		tell application "Finder"
			set firstFile to (first item of startup disk) as alias
		end tell
		set a file to firstFile
	end tell
	
	try
		-- this command should lead to an error because it is targeting the wrong class (it only works on listboxes, not on the app)
		-- to make this an error is generated, the class for this command in the sdef needs
		-- to be defined explicitly (do not leave it empty!) with an Action event handler that
		-- sets the error code. See the "BadCommand" for this.
		reload
	end try
	
	tell first window
		get some raw bytes
		set some raw bytes to «data Raw 1133»
	end tell
	
	set theID to id of listbox 2 of window 1
	get properties of listbox id theID of window 1
	get properties of control id theID of window 1
	
	set np to make new person with properties {name:"Paul", gender:"m", age:34}
	get properties of np
	make new city at beginning with properties {name:"Bonn", country name:"Germany", area:141, population:314000}
	get properties of result
	get name of cities
	delete city 1
	
	get name of every city whose country name is "Germany"
end tell
