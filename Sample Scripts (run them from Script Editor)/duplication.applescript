(*
 * This is a test to demonstrate how the duplicate command returns the
 * copied items. Originally, make, open, move and duplicate are all supposed
 * to return the new or modified items, but Apple's Cocoa Scripting framework
 * only does this for the make command - and that's considered a bug that's
 * long known but never fixed by Apple.
 * Tempelmann's Scriptability classes for Xojo attempt to solve this for the
 * duplicate command. To use the fixed version, the sdef for the move command's
 * Cocoa Class has to be changed from "NSCloneCommand" to "ReturningCloneCommand"
 * and its result type changed to "list of any".
 * The Action event of ReturningCloneCommand sets up an object that will collect
 * all created scriptable objects, calls the original NSCloneCommand's handler
 * and then takes the collected items and returns them as an array.
 *)tell application id "org.tempel.xojo.demo.scriptable" -- AppleScriptable.debug.app	delete every person	-- create two objects	set p1 to make new person with properties {name:"Paul", gender:"m", age:34}	set p2 to make new person at beginning with properties {name:"Ines", gender:"f", age:30}	-- check the ids of the two objects	get id of persons --> 2, 1	-- duplicate the two objects without the "to" parameter	set res1 to duplicate (every person) --> 3, 4	get id of persons --> 2, 3, 1, 4	-- duplicate the two objects again, this time with the "to" parameter	set res2 to duplicate (persons 1 thru 2) to end --> 5, 6	get id of persons --> 2, 3, 1, 4, 5, 6end tell