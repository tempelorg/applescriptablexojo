tell application "AppleScriptable.debug"
	--	delete every city
	
	--	delete (every city whose name is not "bob")
	
	(*
	--	get name of every city
	try
		repeat with x in (get cities)
			delete x
		end repeat
	end try
	get cities
	*)
	
	repeat with x in (get countries)
		get name of x
		delete x
	end repeat
	get name of countries -- Code creates a new array if countries are empty!
	
	move first country to end
	get name of countries -- Code creates a new array if countries are empty!
	
	move country first country to end
	get name of countries -- Code creates a new array if countries are empty!
	
	move country last country to beginning
	get name of countries -- Code creates a new array if countries are empty!
	
end tell
