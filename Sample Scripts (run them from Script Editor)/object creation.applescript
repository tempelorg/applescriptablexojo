tell application id "org.tempel.xojo.demo.scriptable" -- AppleScriptable.debug"
	delete persons
	set n to 1
	repeat 3 times
		make new person with properties {name:"Person " & n}
		set n to n + 1
		make new person with data "Person " & n
		set n to n + 1
	end repeat
	get persons
end tell
