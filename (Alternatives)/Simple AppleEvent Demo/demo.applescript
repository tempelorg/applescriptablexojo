tell application "AETest.debug"
	set res1 to addtwo 5 -- gives 7
	set res2 to average {1.7, 2.2, 3.69} -- gives 2.53
	display dialog makeupcase "upper case" -- shows "UPPER CASE"
end tell
