# AppleScriptableXojo

## Cocoa AppleScript layer for [Xojo](http://xojo.com)

This project includes a set of smart classes that provide several ways to add AppleScript support to a macOS program made with Xojo:

* Make Xojo objects accessible to scripting by subclassing them from classes such as *Scriptable.ObjectProxy*.
* Automatically make any non-private Property of a Class accessible to AppleScripts, through [Introspection](https://docs.xojo.com/index.php/Introspection).
* Fine tune access to properties, including arrays, via Class Events.
* Handle custom commands you've added to your app's scripting dictionary.
* Invoke individual handlers (functions) in AppleScripts.

## Demo Instructions

1. Open the project file *AppleScriptable.rbp* in the Xojo IDE (also supports Real Studio 2012r2.1) and run the project to see a scriptable demo application.
2. Open the scripts from the *Sample Scripts* folder to try out various scripting accessors and commands.
3. Click the "Run" buttons at the bottom of the sample app's window to invoke custom handler in an AppleScript and even invoke a JXA (scripts written JavaScript).
4. Have a look at the Scripting Dictionary of the demo app using **Script Editor** or **[Script Debugger](https://latenightsw.com)**.
5. Open *Resources/AppleScriptable.sdef* in [Sdef Editor](https://www.shadowlab.org/softwares/sdefeditor.php) and identify the connections between classes, properties and commands defined in the Dictionary vs. the sdef file to see how their relate (e.g: "city" class in AppleScript <-> "ScriptableCity" in Xojo project).

## Making your own app scriptable

* Using the IDE, copy the entire *Scriptable* module from *AppleScriptable.rbp* into your project.
* Create a subclass of *Scriptable.AppProxy* (e.g. copy the "ScripableApp" class from the demo prj). This is the root of your app's scriptable data model. Add properties or fill in Event handlers accordingly.
* Create subclasses of *Scriptable.ObjectProxy* for additional scriptable objects, and subclasses of *Scriptable.Command* for additional scriptable commands.
* Register every scriptable class and command, including your *Scriptable.AppProxy* subclass, in your App.Open() Event handler.
* Create an .sdef file with **Sdef Editor**, e.g. by starting from *Unnamed.sdef*, which will become the app's dictionary. Include the .sdef file into your built app, and also add two keys to your app's Info.plist as it's shown in the *Info-addon.plist* file, which gets added by the *AddPlist* Build Automation Step in the demo project.
* Run the program with the included .sdef at least once before you can write scripts for it as the Scripting Dictionary has to be made known first.
* Have a look at the notes in the Scriptable module for additional information, especially the one titled "Important Rules to Follow".

If something doesn't work, compare your project and sdef with the demo's until you figure out what you did wrong.

## License

For now, the code is under Copyright 2016, Thomas Tempelmann, with all rights reserved.

To use this code in your own projects for more than evaluation of this code, you need to acquire an explicit permission by Thomas Tempelmann, tempelmann@gmail.com
